export class CommentPayload {
  duration: string;
  postId: number;
  text: string;
  userName?: string;
  id: number;
}

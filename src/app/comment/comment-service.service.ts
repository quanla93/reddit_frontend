import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {CommentPayload} from "./comment-payload";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  postComment(commentPayload: CommentPayload): Observable<any>  {
    return this.http.post<any>('http://localhost:8080/api/comments', commentPayload);
  }

  getAllCommentsForPost(postId: number): Observable<CommentPayload[]>  {
    return this.http.get<CommentPayload[]>('http://localhost:8080/api/comments/by-post/' + postId);
  }

  getAllCommentsByUser(name: string): Observable<CommentPayload[]> {
    return this.http.get<CommentPayload[]>('http://localhost:8080/api/comments/by-user/' + name);
  }

  deleteCommentById(id: number): Observable<CommentPayload> {
    return this.http.delete<CommentPayload>('http://localhost:8080/api/comments/'+id);
  }

  getCommentById(id: number): Observable<CommentPayload> {
    return this.http.get<CommentPayload>('http://localhost:8080/api/comments/'+id);
  }


  updateComment(commentPayload: CommentPayload): Observable<CommentPayload>  {
    return this.http.put<CommentPayload>('http://localhost:8080/api/comments', commentPayload);
  }
}

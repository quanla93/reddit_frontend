import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { faComments } from '@fortawesome/free-solid-svg-icons';
import {PostModel} from "../post-model";
import {PostService} from "../post.service";
import {Router} from "@angular/router";
import {AuthService} from "../../auth/shared/auth.service";
import {Subject, takeUntil} from "rxjs";
import {Toast, ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-post-tile',
  templateUrl: './post-tile.component.html',
  styleUrls: ['./post-tile.component.css']
})
export class PostTileComponent implements OnInit, OnDestroy{


  @Input() posts: PostModel[];
  faComments = faComments;
  currentUser: string;
  destroy$ = new Subject<void>();
  @Input('searchText') searchText: string;

  constructor(private postService: PostService, private  router: Router, private authService: AuthService, private toastr: ToastrService) {
  }


  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
  ngOnInit(): void {
    this.currentUser = this.authService.getUserName();
  }

  goToPost(id: number): void {
    this.router.navigateByUrl('/view-post/'+id);
  }

  deletePost(id: number): void {
    if(confirm('delete that?') == true){
      this.postService.deletePost(id)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(data => {
        this.toastr.success('Delete success');
        this.GetAllPost();
        console.log(data)
      })
    }
  }

  private GetAllPost() {
    this.postService.getAllPosts()
      .pipe(
      takeUntil(this.destroy$)
    )
      .subscribe(data => {
      this.posts = data;
    })
  }

  gotoUpdatePost(id: number): void {
    this.router.navigateByUrl('/update-post/'+id);
  }

}

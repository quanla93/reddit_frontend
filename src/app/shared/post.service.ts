import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {PostModel} from "./post-model";
import {CreatePostPayload} from "../post/create-post/create-post.payload";
import {UpdatePostPayload} from "../post/update-post/update-post.payload";

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  getAllPosts(): Observable<PostModel[]> {
    return this.http.get<PostModel[]>('http://localhost:8080/api/posts');
  }

  createPost(postPayload: CreatePostPayload): Observable<CreatePostPayload> {
    return this.http.post<CreatePostPayload>('http://localhost:8080/api/posts', postPayload);
  }

  getPost(postId: number): Observable<PostModel> {
    return this.http.get<PostModel>('http://localhost:8080/api/posts/'+postId);
  }

  getPostByUsername(username: string): Observable<PostModel[]> {
    return this.http.get<PostModel[]>('http://localhost:8080/api/posts/by-user/'+username);
  }

  deletePost(id: number): Observable<PostModel> {
    return this.http.delete<PostModel>('http://localhost:8080/api/posts/'+ id);
  }

  updatePost(updatepost: UpdatePostPayload): Observable<UpdatePostPayload>  {
    return this.http.put<UpdatePostPayload>('http://localhost:8080/api/posts', updatepost);
  }

  getPostBySubreddit(id: number): Observable<PostModel[]> {
    return this.http.get<PostModel[]>('http://localhost:8080/api/posts/by-subreddit/'+id);
  }
}

export class CreatePostPayload {
  description: string;
  subredditName?: string;
  url?: string;
  postName: string;

}

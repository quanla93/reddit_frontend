import { Component, OnInit } from '@angular/core';
import {SubredditService} from "../../subreddit/subreddit.service";
import {SubredditModel} from "../../subreddit/subreddit-response";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {CreatePostPayload} from "./create-post.payload";
import {throwError} from "rxjs";
import {PostService} from "../../shared/post.service";

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  subreddits: Array<SubredditModel>;
  createPostForm: FormGroup;
  postPayload: CreatePostPayload;

  constructor(private subredditservice: SubredditService, private router: Router, private postService: PostService) {
    this.postPayload = {
      postName: '',
      description: '',
      url: '',
      subredditName: ''
    }
  }

  ngOnInit(): void {
    this.createPostForm = new FormGroup({
      postName: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      url: new FormControl('', Validators.required),
      subredditName: new FormControl('', Validators.required)
    });
    this.subredditservice.getAllSubreddit().subscribe(data => {
      this.subreddits = data;
    }, error => {
      throwError(error);
    });
  }

  createPost() {
    this.postPayload.postName = this.createPostForm.get('postName')?.value;
    this.postPayload.description = this.createPostForm.get('description')?.value;
    this.postPayload.url = this.createPostForm.get('url')?.value;
    this.postPayload.subredditName = this.createPostForm.get('subredditName')?.value;

    this.postService.createPost(this.postPayload).subscribe(data => {
      this.router.navigateByUrl('/');
    }, error => {
      throwError(error);
    })
  }

  discardPost() {
    this.router.navigateByUrl('/')
  }
}

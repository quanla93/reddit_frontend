import {Component, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {PostModel} from "../../shared/post-model";
import {PostService} from "../../shared/post.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Subject, takeUntil, throwError} from "rxjs";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CommentPayload} from "../../comment/comment-payload";
import {CommentService} from "../../comment/comment-service.service";
import {AuthService} from "../../auth/shared/auth.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.css']
})
export class ViewPostComponent implements OnInit, OnChanges, OnDestroy{

  destroy$ = new Subject<void>();
  currentUser: string;
  postId: number;
  post: PostModel;
  commentForm: FormGroup;
  commentPayload: CommentPayload;
  comments: CommentPayload[];
  editText: string;
  isEditComment: boolean;

  constructor(private postService: PostService, private activateRoute: ActivatedRoute
              ,  private commentService: CommentService, private router: Router
              , private authService: AuthService, private toastr: ToastrService)
  {
    this.postId = this.activateRoute.snapshot.params.id;
    this.commentForm = new FormGroup({
      text: new FormControl('', Validators.required)
    });
    this.commentPayload = {
      duration: '',
      text: '',
      id: 0,
      postId: this.postId
    }
    this.currentUser = this.authService.getUserName();
  }

  ngOnInit(): void {
    this.getPostById();
    this.getCommentsForPost();
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  postComment() {
    this.commentPayload.text = this.commentForm.get('text')?.value;
    this.commentService.postComment(this.commentPayload).subscribe(data => {
      this.commentForm.get('text')?.setValue('');
      this.getCommentsForPost();
    }, error => {
      throwError(error);
    })
  }
  private getPostById() {
    this.postService.getPost(this.postId).subscribe(data => {
      this.post = data;
    }, error => {
      throwError(error);
    });
  }

  private getCommentsForPost() {
    this.commentService.getAllCommentsForPost(this.postId).subscribe(data => {
      this.isEditComment = false;
      this.comments = data;
    }, error => {
      throwError(error);
    });
  }

  deleteComment(id: number) {
      if (id == null){
        return;
      }
      if(confirm('delete that?') == true) {
        this.commentService.deleteCommentById(id)
          .pipe(
            takeUntil(this.destroy$)
          ).subscribe(data => {
          this.toastr.success('delete success');
          this.getCommentsForPost();
          console.log(data);
        })
      }
  }

  UpdateComment(comment: CommentPayload) {
    // this.commentService.getCommentById(comment.id).subscribe(data => {
    //   this.comment = data;
    //   this.editText = comment.text;
    //   this.isEditComment =true;
    // })
    this.comments.forEach(cmt => {
      if(cmt.id == comment.id ){
        this.isEditComment =true;
        this.editText = comment.text;
        this.commentPayload = comment;
      }
    })
  }

  saveEdit() {
    this.commentPayload.text = this.editText;
    this.commentService.updateComment(this.commentPayload).subscribe(data => {
      this.toastr.success('update success');
      this.getCommentsForPost();
      console.log(data);
    })
  }

}

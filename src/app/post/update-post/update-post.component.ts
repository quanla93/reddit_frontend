import {Component, OnDestroy, OnInit} from '@angular/core';
import {SubredditModel} from "../../subreddit/subreddit-response";
import {SubredditService} from "../../subreddit/subreddit.service";
import {ActivatedRoute, Router} from "@angular/router";
import {PostService} from "../../shared/post.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UpdatePostPayload} from "./update-post.payload";
import {Subject, takeUntil, throwError} from "rxjs";
import {PostModel} from "../../shared/post-model";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-update-post',
  templateUrl: './update-post.component.html',
  styleUrls: ['./update-post.component.css']
})
export class UpdatePostComponent implements OnInit, OnDestroy{

  destroy$ = new Subject<void>();
  postId: number;
  post: PostModel;
  subreddits: Array<SubredditModel>;
  updatePostForm: FormGroup;
  updatepost: UpdatePostPayload;


  constructor(private subredditService: SubredditService, private router: Router
              , private postService: PostService, private activateRoute: ActivatedRoute , private toastr: ToastrService) {
      this.postId = this.activateRoute.snapshot.params.id;
    this.updatepost = {
      description: '',
      postId: this.postId,
      postName: '',
      subredditName: '',
      url: ''
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  ngOnInit(): void {
   this.setFormGroup();
   this.setDataForm();
   this.getSubreddit();
  }

  setFormGroup() {
    this.updatePostForm = new FormGroup({
      postName: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      url: new FormControl('', Validators.required),
      subredditName: new FormControl('', Validators.required)
    });
  }
  getSubreddit(){
    this.subredditService.getAllSubreddit()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(data => {
        this.subreddits = data;
      }, error => {
        throwError(error);
      });
  }

  setDataForm() {
    if (!this.postId) {
      return;
    }
    this.postService.getPost(this.postId)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(data => {
        if (!data) {
          return
        }
        this.post = data;
        const {description, postName, url, subredditName} = data;
        this.updatePostForm.patchValue({
          postName, description, url, subredditName
        });
        console.log(this.updatePostForm);
      }, error => {
        throwError(error);
      });
  }

  updatePost() {
    this.updatepost.postName = this.updatePostForm.get('postName')?.value;
    this.updatepost.description = this.updatePostForm.get('description')?.value;
    this.updatepost.url = this.updatePostForm.get('url')?.value;
    this.updatepost.subredditName = this.updatePostForm.get('subredditName')?.value;

    this.postService.updatePost(this.updatepost)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(data => {
        this.toastr.success('Update success');
      this.router.navigateByUrl('/');
    }, error => {
        this.toastr.error('error')
      throwError(error);
    })

  }
  discardPost() {
    this.router.navigateByUrl('/')
  }

}

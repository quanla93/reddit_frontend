export class UpdatePostPayload {
  description: string;
  postId: number;
  postName: string;
  subredditName?: string;
  url?: string;

}

import {Component, OnInit, ViewChild} from '@angular/core';
import {PostModel} from "../shared/post-model";
import {PostService} from "../shared/post.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // @ViewChild('searchComponent', {static: true}) searchPost: SearchComponent;
  posts: Array<PostModel> = [];
  searchText: string;
  constructor(private postService: PostService) {
    this.postService.getAllPosts().subscribe(data => {
      this.posts = data;
    })
  }

  ngOnInit(): void {
    // this.searchText = this.searchPost.searchText;

  }

  search(text: string) {
    this.searchText = text;
    console.log(text)
  }
}

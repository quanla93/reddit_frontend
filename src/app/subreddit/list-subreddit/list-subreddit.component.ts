import { Component, OnInit } from '@angular/core';
import {SubredditService} from "../subreddit.service";
import {SubredditModel} from "../subreddit-response";
import {throwError} from "rxjs";

@Component({
  selector: 'app-list-subreddit',
  templateUrl: './list-subreddit.component.html',
  styleUrls: ['./list-subreddit.component.css']
})
export class ListSubredditComponent implements OnInit {

  subreddits: Array<SubredditModel>;

  constructor(private subredditService: SubredditService) {
  }

  ngOnInit(): void {
    this.subredditService.getAllSubreddit().subscribe(data => {
      this.subreddits = data;
    }, error => {
      throwError(error);
    });
  }


}

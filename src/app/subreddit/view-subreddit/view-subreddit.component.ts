import {Component, OnDestroy, OnInit} from '@angular/core';
import {PostService} from "../../shared/post.service";
import {PostModel} from "../../shared/post-model";
import {AuthService} from "../../auth/shared/auth.service";
import {ActivatedRoute} from "@angular/router";
import {Subject, takeUntil} from "rxjs";

@Component({
  selector: 'app-view-subreddit',
  templateUrl: './view-subreddit.component.html',
  styleUrls: ['./view-subreddit.component.css']
})
export class ViewSubredditComponent implements OnInit, OnDestroy {

  destroy$ = new Subject<void>();
  id: number;
  posts: PostModel[];
  constructor(private postService: PostService, private activateRoute: ActivatedRoute) {
    this.id = this.activateRoute.snapshot.params.id;
  }

  ngOnInit(): void {
    this.getPostBySubreddit();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getPostBySubreddit(){
    this.postService.getPostBySubreddit(this.id)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(data => {
      this.posts = data;
    })
  }

}

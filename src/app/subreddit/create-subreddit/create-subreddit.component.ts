import { Component, OnInit } from '@angular/core';
import {SubredditModel} from "../subreddit-response";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {SubredditService} from "../subreddit.service";
import {error} from "@angular/compiler-cli/src/transformers/util";

@Component({
  selector: 'app-create-subreddit',
  templateUrl: './create-subreddit.component.html',
  styleUrls: ['./create-subreddit.component.css']
})
export class CreateSubredditComponent implements OnInit {

  subreddit: SubredditModel;
  createSubredditForm: FormGroup;

  title = new FormControl('');
  description = new FormControl('');
  constructor(private router: Router, private subredditService: SubredditService) {
    this.createSubredditForm = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });
    this.subreddit = {
      name: '',
      description: ''
    }
  }

  ngOnInit(): void {
  }


  discard() {
    this.router.navigateByUrl('/');
  }


  createSubreddit() {
    this.subreddit.name = this.createSubredditForm.get('title')?.value;
    this.subreddit.description = this.createSubredditForm.get('description')?.value;
    this.subredditService.createSubreddit(this.subreddit).subscribe(data => {
      this.router.navigateByUrl('/list-subreddits');
    }, error => {
      console.log('error occurred');
    })
  }
}

import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PostService} from "../../shared/post.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchText: string;

  @Output() searchTextChange = new EventEmitter<string>();
  constructor() {

  }

  ngOnInit(): void {
  }

  searchTextchange(){
    this.searchTextChange.emit(this.searchText);
  }
}
